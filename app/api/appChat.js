var TelegramBot = require('node-telegram-bot-api');
var token = '362820575:AAEPKJFAlc1ELnfXziqUR2G7y0wUDnt8BIY';
var telebot = new TelegramBot(token, { polling: true });
var http = require('http');
var express = require('express');
var EventEmitter = require('events').EventEmitter;
var serverEmitter = new EventEmitter();
var user;
var PORT = 8008;

var app = express();
var server = http.createServer(app);
var io = require('socket.io').listen(server);
server.listen(PORT);

app.use('/static', express.static(__dirname + '/static'));

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

io.sockets.on('connection', function (socket) {
    console.log('********************************* NEW CONNECTION *************************************');
    serverEmitter.on('newFeed', function (data) {
        console.log('gonna emit');
        socket.emit('message', data);
    });
    socket.on('message', function (message) {
        try {
            if (user) {
                socket.emit("message", message);
                telebot.sendMessage(user, message.message);
            }
        } catch (e) {
            console.log(e);
            client.disconnect();
        }
    });
});


telebot.on('message', function (msg) {
    console.log('********************************* NEW MESSAGE *************************************');
    try {
        user = msg.chat.id;
        var text = {
            'message': msg.text, 'name': msg.from.first_name
            + ' ' + msg.from.last_name
        };
        serverEmitter.emit('newFeed', text);
    } catch (e) {
        console.log(e);
        socket.disconnect();
    }
});
